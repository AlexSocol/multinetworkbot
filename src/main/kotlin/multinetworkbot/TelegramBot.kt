package multinetworkbot

import org.telegram.telegrambots.ApiContextInitializer
import org.telegram.telegrambots.bots.TelegramLongPollingBot
import org.telegram.telegrambots.meta.ApiContext
import org.telegram.telegrambots.meta.TelegramBotsApi
import org.telegram.telegrambots.meta.api.methods.send.SendMediaGroup
import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto
import org.telegram.telegrambots.meta.api.objects.Update
import org.telegram.telegrambots.meta.api.objects.media.InputMediaPhoto
import org.telegram.telegrambots.meta.bots.AbsSender
import org.telegram.telegrambots.meta.exceptions.TelegramApiException
import org.telegram.telegrambots.meta.generics.BotSession
import java.io.File
import kotlin.properties.Delegates

class MultiNetworkBotTelegram : MultiNetworkBot {
	
	private val session: BotSession
	private val bot: AbsSender
	private var name: String by Delegates.notNull()
	private var token: String by Delegates.notNull()
	private var channelEn: String by Delegates.notNull()
	private var channelRu: String by Delegates.notNull()
	
	private var inited = false
	
	init {
		log("Initializing Telegram context...")
		ApiContextInitializer.init()
		log("Success.")
		
		log("Initializing Telegram bot...")
		init()
		log("Success.")
		
		val botsApi = TelegramBotsApi()
		bot = MultiNetworkTelegramLongPollingBot(name, token)
		var stupidKotlin: BotSession
		
		try {
			log("Registering Telegram bot...")
			stupidKotlin = botsApi.registerBot(bot)
			log("Success.")
		} catch (e: TelegramApiException) {
			stupidKotlin = ApiContext.getInstance(BotSession::class.java)
			err("Exception in Telegram bot registration: ")
			e.printStackTrace()
		}
		
		session = stupidKotlin
	}
	
	override fun init(): MultiNetworkBot {
		if (inited) return this
		
		log("Reading Telegram bot data...")
		val reader = File("bots/Telegram.txt").bufferedReader()
		
		name = reader.readLine()
		token = reader.readLine()
		channelEn = reader.readLine()
		channelRu = reader.readLine()
		
		reader.close()
		log("Done.")
		
		inited = true
		
		return this
	}
	
	override fun start(): MultiNetworkBot {
		if (!active()) session.start()
		
		return this
	}
	
	override fun active() = session.isRunning
	
	override fun stop(): MultiNetworkBot {
		if (active()) session.stop()
		return this
	}
	
	override fun send() = when (MNBOperator.getPhotoList().size) {
		0 -> exec(msgRu()) && exec(msgEn())
		1 -> exec(imgRu()) && exec(imgEn())
		in 2..10 -> exec(mulRu()) && exec(mulEn())
		else -> false
	}
	
	fun exec(method: Any): Boolean {
		try {
			when (method) {
				is SendMessage -> bot.execute(method)
				is SendPhoto -> bot.execute(method)
				is SendMediaGroup -> bot.execute(method)
				else -> return false
			}
			return true
		} catch (e: TelegramApiException) {
			e.printStackTrace()
			return false
		}
	}
	
	fun msgRu() = SendMessage()												.setText	(MNBOperator.getMessageRu()).setChatId(channelRu)
	fun msgEn() = SendMessage()												.setText	(MNBOperator.getMessageEn()).setChatId(channelEn)
	fun imgRu() = SendPhoto()		.setPhoto(MNBOperator.getPhotoList()[0]).setCaption	(MNBOperator.getMessageRu()).setChatId(channelRu)
	fun imgEn() = SendPhoto()		.setPhoto(MNBOperator.getPhotoList()[0]).setCaption	(MNBOperator.getMessageEn()).setChatId(channelEn)
	fun mulRu() = SendMediaGroup()	.setMediaInline(media())				.setCaption	(MNBOperator.getMessageRu()).setChatId(channelRu)
	fun mulEn() = SendMediaGroup()	.setMediaInline(media())				.setCaption	(MNBOperator.getMessageEn()).setChatId(channelEn)
	fun media() = MNBOperator.getPhotoList().map { file -> InputMediaPhoto().setMedia(file, file.getName()) }
}

class MultiNetworkTelegramLongPollingBot(val userName: String, val token: String) : TelegramLongPollingBot() {
	
	override fun getBotUsername() = userName
	override fun getBotToken() = token
	
	override fun onUpdateReceived(update: Update) {
		if (update.hasMessage() && update.message.hasText()) {
			log("<${update.message.from.userName}>: ${update.message.text}")
		}
		
		if (update.hasChannelPost() && update.channelPost.hasText()) {
			val sender = if (update.channelPost.chat.firstName != null) update.channelPost.chat.firstName else "[${update.channelPost.chatId}]"
			log("<$sender>: ${update.channelPost.text}")
			
			val message = SendMessage().setChatId(update.channelPost.chatId).setText("Your message:\n${update.channelPost.text}")
			
			try {
				execute(message)
			} catch (e: TelegramApiException) {
				e.printStackTrace()
			}
		}
	}
}