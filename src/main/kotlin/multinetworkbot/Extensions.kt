package multinetworkbot

import org.telegram.telegrambots.meta.api.methods.send.SendMediaGroup
import org.telegram.telegrambots.meta.api.objects.media.InputMedia

fun SendMediaGroup.setCaption(caption: String): SendMediaGroup {
	getMedia().get(0)?.setCaption(caption)
	return this
}

fun SendMediaGroup.getCaption() = getMedia().get(0)?.getCaption()

fun <T> SendMediaGroup.setMediaInline(media: List<InputMedia<T>>): SendMediaGroup {
	setMedia(media)
	return this
}