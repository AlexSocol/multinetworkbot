package multinetworkbot

import java.io.BufferedReader
import java.io.File
import java.io.InputStreamReader
import java.text.SimpleDateFormat
import java.util.Calendar
import kotlin.properties.Delegates

val cin = BufferedReader(InputStreamReader(System.`in`))
val calendar = Calendar.getInstance()
val sdf = SimpleDateFormat("HH:mm:ss")

fun log(obj: Any?) = println("[${sdf.format(calendar.getTime())}] [${Thread.currentThread().name}/INFO] $obj")
fun err(obj: Any?) = System.err.println("[${sdf.format(calendar.getTime())}] [${Thread.currentThread().name}/ERROR] $obj")

fun main(args: Array<String>) {
	log("Starting...")
	MNBOperator.instance().start()
}

class MNBOperator {
	
	val bots = HashMap<String, MultiNetworkBot>()
	var messageEn: String by Delegates.notNull()
	var messageRu: String by Delegates.notNull()
	var photoList: Array<File> by Delegates.notNull()
	
	companion object {
		private val singleton = MNBOperator()
		fun instance() = singleton
		
		fun getMessageRu() = instance().messageRu
		fun getMessageEn() = instance().messageEn
		fun getPhotoList() = instance().photoList
	}
	
	fun start() {
		init()
		configure()
		run()
	}
	
	fun init() {
		log("Initializing and registering bots...")
		bots.put("Telegram", MultiNetworkBotTelegram().init())
		// <-- More bots goes here
		
		log("All bots registered: ${bots.keys}.")
	}
	
	fun configure() {
		log("Reading messages...")
		messageEn = File("data/MessageEn.txt").readText()
		messageRu = File("data/MessageRu.txt").readText()
		log("Done.")
		
		log("Reading images...")
		photoList = File("data/images").listFiles()
		log("Done: found ${if (photoList.size > 0) photoList.size.toString() else "no"} images.")
	}
	
	fun run() {
		log("Starting all [inactive] registered bots..")
		bots.forEach { _, bot -> bot.start() }
		log("Success.")
		
		log("Starting commandline input handler...")
		
		var read: String
		log("Done. Waiting for inputs...")
		
		cycle@ while (true) {
			try {
				read = cin.readLine()
				
				when {
					read.equals("list") -> log("Active bots: ${bots.keys}")
					
					read.equals("start") -> start(read.split(" ")[1])
					read.equals("ping") -> ping(read.split(" ")[1])
					read.startsWith("shut") -> shutDown(read.split(" ")[1])
					
					read.equals("post") -> bots.forEach {_, bot -> bot.send()}
					
					read.equals("exit") -> {
						log("Received \"$read\" command. Shutting down all bots...")
						bots.forEach { name, _ -> shutDown(name) }
						break@cycle
					}
					
				}
				
				Thread.sleep(500)
			} catch (e: Throwable) {
				err("Exception in input reader:")
				e.printStackTrace()
			}
		}
		
		log("Exiting programm...")
		
		System.exit(0)
	}
	
	fun start(name: String) {
		log("Starting $name bot...")
		val bot = bots[name]
		
		if (bot != null) {
			bot.start()
			log("Success.")
		} else log("No such bot.")
	}
	
	fun ping(name: String) {
		log("Trying to reach $name bot...")
		val bot = bots[name]
		
		if (bot != null) {
			log("Bot is ${if (bot.active()) "" else "in"}active.")
		} else log("No such bot.")
	}
	
	fun shutDown(name: String) {
		log("Shutting down $name bot...")
		val bot = bots[name]
		
		if (bot != null) {
			bot.stop()
			log("Success.")
		} else log("No such bot.")
	}
}

abstract interface MultiNetworkBot {
	
	abstract fun init(): MultiNetworkBot
	
	abstract fun start(): MultiNetworkBot
		
	abstract fun active(): Boolean
	
	abstract fun stop(): MultiNetworkBot
	
	abstract fun send(): Boolean
}